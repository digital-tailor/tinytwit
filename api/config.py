import os


class Config(object):
    API_TITLE = 'TinyTwit'
    API_VERSION = 'v1'
    APP_VERSION = os.environ.get('APP_VERSION', 'local')
    LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')
    TIME_FORMAT = '%Y-%m-%dT%H:%M:%S+00:00'
    OPENAPI_VERSION = "3.0.2"
    OPENAPI_JSON_PATH = "api-spec.json"
    OPENAPI_URL_PREFIX = "/"
    OPENAPI_SWAGGER_UI_PATH = "/swagger-ui"
    OPENAPI_SWAGGER_UI_URL = "https://cdn.jsdelivr.net/npm/swagger-ui-dist/"

