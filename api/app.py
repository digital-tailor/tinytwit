from flask import Flask

from api.config import Config
from api.extensions import api
from api.resources.health import bp as health_bp


def create_app():
    app = Flask('tinytwit-api')
    app.url_map.strict_slashes = False
    app.config.from_object(Config)
    api.init_app(app)
    # Register blueprints
    api.register_blueprint(health_bp)
    return app
