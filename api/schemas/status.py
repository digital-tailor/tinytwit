from marshmallow import fields, Schema, EXCLUDE

from api.config import Config


class Status(Schema):
    class Meta:
        ordered = True
        unknown = EXCLUDE

    status = fields.String(
        default='up',
        description='Returns up if the application runs',
        example='up'
    )
    app_version = fields.String(
        default=Config.APP_VERSION,
        description='Application version',
        example='v0.1.0'
    )
