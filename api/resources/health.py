from flask.views import MethodView
from flask_smorest import Blueprint

from api.schemas.status import Status

bp = Blueprint('health', __name__)


@bp.route('/api-health')
class Status(MethodView):

    @bp.response(Status)
    def get(self):
        """Returns app health"""
        return None
