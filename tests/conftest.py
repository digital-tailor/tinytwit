import pytest


def pytest_configure(config):
    # Register custom marks
    config.addinivalue_line(
        "markers", "no_auto_fixture: Do not mock redis but use the normal class"
    )


@pytest.fixture(autouse=True)
def log_level(caplog):
    caplog.set_level('DEBUG')
