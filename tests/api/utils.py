from marshmallow import EXCLUDE


def is_valid_entity(entity_class, data):
    """
    This helper tells if there are validation errors on the data to validate.

    :param entity_class:
    :param data:
    :return: bool
    """
    errors = entity_class(unknown=EXCLUDE).validate(data)
    return len(errors) == 0
