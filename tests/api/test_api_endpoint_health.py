import pytest

from api.schemas.status import Status
from tests.api.utils import is_valid_entity


@pytest.mark.parametrize('headers', [
    None,
    {'foo': 'bar'}
])
@pytest.mark.parametrize('endpoint', [
    '/api-health',
    '/api-health/'
])
def test_health_endpoint_is_public(client, endpoint, headers):
    r = client.get(endpoint, headers=headers)
    assert r.status_code == 200
    assert r.is_json
    assert is_valid_entity(Status, r.json)
    assert 'status' in r.json
    assert 'app_version' in r.json
