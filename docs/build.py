# Generates the swagger based on the schemas defined.
# See https://github.com/marshmallow-code/apispec
import importlib
import os
import yaml

# List components for which the docs will be generated
# Once declared, just run this file and output goes in
# docs/{component}/swagger.yml as per the build_docs below.
components = ['api']


def build_doc(component):
    """
    This builds the swagger for a given component.
    Resulting file goes under docs/{component}/swagger.yml
    This assumes that the component has a spec.py file defining the spec for it.
    :param component: name of the python package to generate swagger for.
    """
    try:
        if component == 'api':
            from api.app import create_app
            from api.extensions import api
            create_app()
            spec = api.spec
        else:
            spec = importlib.import_module(f'{component}.spec').spec
        path = f'/{component}/swagger.yml'
        output_file = os.path.dirname(os.path.realpath(__file__)) + path
        os.makedirs(os.path.dirname(output_file), exist_ok=True)
        with open(output_file, "w") as f:
            yaml.dump(yaml.load(spec.to_yaml(), Loader=yaml.FullLoader), f)
        print("Generated swagger in {}".format(output_file))
    except Exception as e:
        print('Could not generate swagger for {}: {}'.format(component, e))


if __name__ == "__main__":
    for c in components:
        build_doc(c)
